def hello(name: str) -> str:
	return "Hello " + name


def bye(name: str) -> str:
    return "Bye " + name

def good_morning(name: str) -> str:
    return "Good morning " + name
